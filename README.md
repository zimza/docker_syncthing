## Build and run
```sh
export USER UID  
cd /home/$USER/syncthing/  
docker-compose up -d --force-recreate --remove-orphans --always-recreate-deps --build  
docker-compose exec -d syncthing syncthing -no-browser -no-restart -home=/home/$USER/syncthing/config
```

## Init at startup
vim /etc/systemd/system/syncthing.service
```sh
[Unit]
Wants=network-online.target
After=network-online.target

[Service]
ExecStart=/bin/bash -c 'export USER UID && cd /home/$USER/syncthing/ && docker-compose up -d --force-recreate --remove-orphans --always-recreate-deps --build && docker-compose exec -d syncthing syncthing -no-browser -no-restart -home=/home/$USER/syncthing/config'

[Install]
WantedBy=default.target
```
