FROM alpine:edge

RUN apk update && apk upgrade
RUN apk add syncthing

ARG USER
ENV USER=$USER

ARG UID
ENV UID=$UID

RUN echo $USER $UID
RUN adduser -Du $UID $USER

USER $USER

RUN mkdir -p /home/$USER/syncthing/config /home/$USER/syncthing/data

RUN chown $USER:$USER /home/$USER/syncthing/*

ENV STGUIADDRESS=0.0.0.0:8384
